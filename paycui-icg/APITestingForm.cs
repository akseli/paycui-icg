﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paycui_icg
{
    public partial class APITestingForm : Form
    {
        static string accessToken = "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MTI1NCwidXNlcm5hbWUiOiJkZW1vQGRlbW8uZGVtbyIsIm5hbWUiOiJEZW1vIiwic3VybmFtZSI6IiIsInBob25lIjoiMzM2NiIsImFkZHJlc3MiOm51bGwsImpvYiI6bnVsbCwiY29sbGVnZSI6bnVsbCwidW5pdmVyc2l0eSI6bnVsbCwic29jaWFsTmV0d29yayI6IiIsInBlcm1pc3Npb25zIjpudWxsLCJyZXN0YXVyYW50IjpudWxsLCJncm91cCI6bnVsbCwicG9zdGFsQ29kZSI6bnVsbCwiZ2VuZGVyIjowLCJiaXJ0aGRheSI6IiIsImxhbmd1YWdlIjpudWxsLCJqb2luZWQiOjE1MTkwNjgzOTA1NzV9.we8VHr7PxNTDDr5REGOk590Z5J4UFLyLdOmUOTiAfBY";
        static string baseUrl = "https://panel.paycui.com/api/v1/";

        static Timer myTimer = new Timer();
        public APITestingForm()
        {
            InitializeComponent();

            myTimer.Tick += new EventHandler(TimerFunction);

            myTimer.Interval = 2000;
            myTimer.Start();
        }

        private static void getUserCommand(int userId)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl + "command/user/" + userId);
                request.ContentType = "application/json; charset=utf8";
                request.Headers.Add("Authorization:" + accessToken);
                request.Method = WebRequestMethods.Http.Get;

                WebResponse response = request.GetResponse();

                StreamReader sr = new StreamReader(response.GetResponseStream());

                Debug.WriteLine(sr.ReadToEnd());

            } catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private static void TimerFunction(Object ob, EventArgs eventArgs)
        {
            getUserCommand(1188);
        }
    }
}
