﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paycui_icg
{
    public partial class DBTestingForm : Form
    {
        string connectionString = "Data Source=192.168.1.46,1433;Network Library=DBMSSOCN; Initial Catalog = HData; User ID = testing; Password=PayCui;";
        public DBTestingForm()
        {
            InitializeComponent();

            ConnectToDb();
        }

        private void ConnectToDb()
        {
            string query = "select * from dbo.Documento WHERE Descripcion='mesa4'";
            string insertQuery = "INSERT INTO dbo.LineaDocumento VALUES ('100000033', '100000007', '8', 'PayCui Remoto', '1.0000', '1.0000', '1.0000', '1,00', '1,00', '0.0000', '156', '0,91', '1,00', '1', '0', '0', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0', '0', '0', '0.0000', '0.0000', '0', 'NULL', 'NULL');";
            string updateQuery = "UPDATE dbo.Documento SET Importe = '4,55', ImporteIVA = '304';";
            SqlDateTime time = new SqlDateTime(2018, 3, 1);
            string createTable = "INSERT INTO dbo.Documento VALUES ('100000008', '1', '100000001', '3', '1', '100000001', '100000001', '1', '1', '" + time.ToString() + "', '611', '0,00', '0,00', '0', 'mesaPayCui', '0', '0', '', '1', '0', '1', '1', '0', '', '', '0', '0', '011', '', '0', '', '', '0,00', '', '', '', '', '0', '0', '0');";

            string fullQuery = insertQuery + updateQuery;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(createTable, connection);

            try
            {
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    Debug.WriteLine(reader.GetValue(11));
                    Debug.WriteLine(reader.GetValue(12));
                    Debug.WriteLine(reader.GetValue(14));
                }

                reader.Close();
                cmd.Dispose();
                connection.Close();
            } catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }
    }
}
